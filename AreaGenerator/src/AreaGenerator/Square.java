package AreaGenerator;

public class Square extends Shape implements TwoDimensionalShapeInterface {

	private String Colour;
	private int Sides;
	private int Dimensions;
	private Double Length;
	
	public Square(String colour,int sides, int dimensions, Double length) {
		super(colour, sides, dimensions, length, dimensions, dimensions);
		this.Colour = colour;
		this.Sides = sides;
		this.Dimensions = dimensions;
		this.Length = length;
	}
	
	//getters and setters 
	public String getColour() {
		return Colour;
	}

	public void setColour(String colour) {
		Colour = colour;
	}

	public int getSides() {
		return Sides;
	}

	public void setSides(int sides) {
		Sides = sides;
	}

	public int getDimensions() {
		return Dimensions;
	}

	public void setDimensions(int dimensions) {
		Dimensions = dimensions;
	}

	public Double getLength() {
		return Length;
	}

	public void setLength(Double length) {
		Length = length;
	}
	
	@Override
	public double calculateArea() {
		// TODO Auto-generated method stub
		double Result = Sides * Sides;
		return Result;
	
	}


	@Override
	public void printInfo() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void display() {
		// TODO Auto-generated method stub
		System.out.println("The Area of the Square is : " +calculateArea() + " cm");
		System.out.println("The Colour of the Square is : " +Colour);
		System.out.println("The Length of the Square is : " +Length + " cm");
		

	
	}

}
