package AreaGenerator;

public interface ThreeDimensionalShapeInterface {
	public double calculateVolume();
	public double printDetails();
	public void display();
}
