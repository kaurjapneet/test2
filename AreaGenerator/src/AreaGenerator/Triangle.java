package AreaGenerator;

public class Triangle extends Shape implements TwoDimensionalShapeInterface,ThreeDimensionalShapeInterface {

	private String Colour;
	private int Sides;
	private int Base;
	private int Height;
	private Double Length;
	
	public Triangle(String colour, int sides, int base, int height, Double length) {
		super(colour, sides, height, length, base, height);
		this.Colour = colour;
		this.Sides = sides;
		this.Base = base;
		this.Height = height;
		this.Length = length;
	}

	//getters and setters
	public String getColour() {
		return Colour;
	}

	public void setColour(String colour) {
		Colour = colour;
	}

	public int getSides() {
		return Sides;
	}

	public void setSides(int sides) {
		Sides = sides;
	}

	public int getBase() {
		return Base;
	}

	public void setBase(int base) {
		Base = base;
	}

	public int getHeight() {
		return Height;
	}

	public void setHeight(int height) {
		Height = height;
	}

	public Double getLength() {
		return Length;
	}

	public void setLength(Double length) {
		Length = length;
	}

	@Override
	public double calculateArea() {
		// TODO Auto-generated method stub
		double Result = 0.5 * Base * Height;
		return Result;
	}

	@Override
	public void printInfo() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public double calculateVolume() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double printDetails() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void display() {
		// TODO Auto-generated method stub
		System.out.println("The Area of the Triangle is : " +calculateArea() + " cm");
		System.out.println("The Colour of the Trinangle is : " +Colour);
		System.out.println("The Length of the Triangle is : " +Length + " cm");
		
	}

}
