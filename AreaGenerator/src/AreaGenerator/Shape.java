package AreaGenerator;

import java.util.ArrayList;

public class Shape {

	private String Colour;
	private int Sides;
	private int Dimensions;
	private Double Length;
	private int Base;
	private int Height;
	
	public Shape(String colour, int sides, int dimensions, Double length, int base, int height) {
		this.Colour = colour;
		this.Sides = sides;
		this.Dimensions = dimensions;
		this.Length = length;
		this.Base = base;
		this.Height = height;
	}

	public String getColour() {
		return Colour;
	}

	public void setColour(String colour) {
		Colour = colour;
	}

	public int getSides() {
		return Sides;
	}

	public void setSides(int sides) {
		Sides = sides;
	}

	public int getDimensions() {
		return Dimensions;
	}

	public void setDimensions(int dimensions) {
		Dimensions = dimensions;
	}

	public Double getLength() {
		return Length;
	}

	public void setLength(Double length) {
		Length = length;
	}

	public int getBase() {
		return Base;
	}

	public void setBase(int base) {
		Base = base;
	}

	public int getHeight() {
		return Height;
	}

	public void setHeight(int height) {
		Height = height;
	}
	
	ArrayList<Shape> ShapeList = new ArrayList<Shape>();
	public void addshape(Shape shape) {
	ShapeList.add(shape);
	}
	
	public void display() {
		
	}

	
}
